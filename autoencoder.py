# https://blog.keras.io/building-autoencoders-in-keras.html
# http://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/
# Michael:  https://github.com/kundajelab/dragonn/blob/master/dragonn/utils.py
# git clone git@gitlab.com:wandreopoulos/read-autoencoder.git

from __future__ import absolute_import, division, print_function
import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d
from keras.utils import np_utils
from keras.preprocessing import sequence


import numpy as np

def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]



x=[]
xp2 = []

#f.close()
#f=open('all.fastq', 'r')
#for l in f.readlines():
#count=1
with open("all.fastq") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 150:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,600))
    #count
    #count += 1
    #x.append(a)
    #print(str(a) + "  %s" % (len(a)))

#xn=np.array(x)
#xn=xn/4.0
#xp = one_hot_encode(np.array(x))
#xp2 = []
#for i in xp:
#    xp2.append(np.reshape(i,600))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=600)
xn[0]
#f.close()




from keras.layers import Input, Dense
from keras.models import Model

# this is the size of our encoded representations
encoding_dim = 450  # 32 floats -> compression of factor 24.5, assuming the input is 784 floats

# this is our input placeholder
input_img = Input(shape=(600,))
# "encoded" is the encoded representation of the input
encoded = Dense(encoding_dim, activation='relu')(input_img)
# "decoded" is the lossy reconstruction of the input
decoded = Dense(600, activation='sigmoid')(encoded)

# this model maps an input to its reconstruction
autoencoder = Model(input_img, decoded)

# this model maps an input to its encoded representation
encoder = Model(input_img, encoded)

# create a placeholder for an encoded (32-dimensional) input
encoded_input = Input(shape=(encoding_dim,))
# retrieve the last layer of the autoencoder model
decoder_layer = autoencoder.layers[-1]
# create the decoder model
decoder = Model(encoded_input, decoder_layer(encoded_input))

autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')


autoencoder.fit(xn, xn,
                nb_epoch=50,
                batch_size=256,
                shuffle=True,
                validation_data=(xn, xn))


# encode and decode some digits
# note that we take them from the *test* set
encoded_imgs = encoder.predict(xn)
decoded_imgs = decoder.predict(encoded_imgs)

scores = autoencoder.evaluate(xn, xn, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))



for i in range(25288):
     pearsonr(decoded_imgs[i], xn[i])
     numpy.corrcoef(decoded_imgs[i], xn[i])




# use Matplotlib (don't ask)
import matplotlib.pyplot as plt

n = 10  # how many digits we will display
plt.figure(figsize=(20, 4))
for i in range(n):
    # display original
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(xn[i].reshape(30, 20))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display reconstruction
    ax = plt.subplot(2, n, i + 1 + n)
    plt.imshow(decoded_imgs[i].reshape(30, 20))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()




>>> x1=[]                                   
>>> y1=[]                                   
>>> for i in range(0, 1000):                
...    x1.append(encoded_imgs[i][0])         
...    y1.append(encoded_imgs[i][1])         
>>> x3=[]
>>> y3=[]
>>> for i in range(2000, 3000):
...    x3.append(encoded_imgs[i][0])
...    y3.append(encoded_imgs[i][1])
>>> plt.plot(x3, y3, "bo")
[<matplotlib.lines.Line2D object at 0x2b23d2c6d250>]
>>> plt.plot(x1, y1, "ro")
[<matplotlib.lines.Line2D object at 0x2b23d2bc0fd0>]
>>> plt.show()

