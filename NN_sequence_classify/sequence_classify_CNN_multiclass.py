#http://machinelearningmastery.com/multi-class-classification-tutorial-keras-deep-learning-library/

import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.preprocessing import sequence
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline



from __future__ import absolute_import, division, print_function
import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d


def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]


x=[]
xp2 = []

#f.close()
#f=open('all.fastq', 'r')
#for l in f.readlines():
#count=1
with open("all.fastq") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 150:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,600))
    #count
    #count += 1
    #x.append(a)
    #print(str(a) + "  %s" % (len(a)))

#xn=np.array(x)
#xn=xn/4.0
#xp = one_hot_encode(np.array(x))
#xp2 = []
#for i in xp:
#    xp2.append(np.reshape(i,600))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=600)
xn[0]
#f.close()



# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# load dataset
#dataframe = pandas.read_csv("iris.csv", header=None)
#dataset = dataframe.values
#X = dataset[:,0:4].astype(float)
X = xn
#Y = dataset[:,4]
#Y = np.ones(len(X))
#TODO check if num classes = the value in model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))
with open("all.categories") as f:
   content = f.readlines()

content = [x.strip() for x in content] 
Y = np.array(content)


# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)


# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(600, input_dim=600, kernel_initializer='normal', activation='relu'))
	model.add(Dense(26, kernel_initializer='normal', activation='sigmoid'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

model = baseline_model()
model.fit(X, dummy_y,
                nb_epoch=50,
                batch_size=256,
                shuffle=True,
                validation_data=(X,dummy_y))
scores = model.evaluate(X, dummy_y, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))


estimator = KerasClassifier(build_fn=baseline_model, epochs=50, batch_size=256, verbose=0)

kfold = KFold(n_splits=10, shuffle=True, random_state=seed)

results = cross_val_score(estimator, X, dummy_y, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))


