# http://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/
# LSTM for sequence classification in the IMDB dataset
# Taken from sequence_classify_CNN_multiclass.py AND sequence_classify.py


from __future__ import absolute_import, division, print_function
import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d

import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.preprocessing import sequence
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.models import load_model

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline

import matplotlib.pyplot as plt






def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]

#________



x=[]
xp2 = []

#f.close()
#f=open('all.fastq', 'r')
#for l in f.readlines():
#count=1
with open("ALL_mock.fasta") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 70:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,280))
    #count
    #count += 1
    #x.append(a)
    #print(str(a) + "  %s" % (len(a)))

#xn=np.array(x)
#xn=xn/4.0
#xp = one_hot_encode(np.array(x))
#xp2 = []
#for i in xp:
#    xp2.append(np.reshape(i,600))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=280)
xn[0]
#f.close()

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# load dataset
#dataframe = pandas.read_csv("iris.csv", header=None)
#dataset = dataframe.values
#X = dataset[:,0:4].astype(float)
X = xn


####Test data
x=[]
xp2 = []
with open("Hirscia.fasta") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 70:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,280))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=280)
X_test = xn
######




#Y = dataset[:,4]
#Y = np.ones(len(X))
#TODO check if num classes = the value in model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))
with open("ALL_mock.classes") as f:
   content = f.readlines()

print("Training ALL_mock.classes")

content = [x.strip() for x in content]
Y = np.array(content)

content2 = [int(x) for x in content]
Y_train = np.array(content2)


# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)

#_________
####Test data
with open("Hirscia.classes") as f:
   content = f.readlines()

print("Testing Hirscia.classes")

content = [x.strip() for x in content]
Y = np.array(content)

content2 = [int(x) for x in content]
Y_test = np.array(content2)


# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y_test = np_utils.to_categorical(encoded_Y)
######

'''
x=[]
xp2 = []

#f.close()
f=open('./test/SEQS.txt', 'r')
for l in f.readlines():
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,600))
    #x.append(a)
    #print(str(a) + "  %s" % (len(a)))

#xn=np.array(x)
#xn=xn/4.0
#xp = one_hot_encode(np.array(x))
#xp2 = []
#for i in xp:
#    xp2.append(np.reshape(i,600))

xn = np.array(xp2)
xn[0]
f.close()
'''

# define baseline model
def baseline_model():
        # create model
        model = Sequential()
        model.add(Embedding(max_features, 128, input_length=maxlen))
        model.add(LSTM(100))
        model.add(Dense(1, activation='sigmoid'))
        # Compile model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model



max_features = 20000
# cut texts after this number of words
# (among top max_features most common words)
maxlen = 280
batch_size = 32

# fix random seed for reproducibility
#numpy.random.seed(7)
# load the dataset but only keep the top n words, zero the rest
#top_words = 5000
#(X_train, y_train), (X_test, y_test) = imdb.load_data(nb_words=top_words)
# truncate and pad input sequences
#max_review_length = 500
#X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
#X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)
# create the model
# embedding_vecor_length = 32
model = Sequential()
#model.add(Embedding(max_features, 128, input_length=maxlen))
###Embedding(top_words, embedding_vecor_length, input_length=max_review_length))
#model.add(LSTM(100))
#model.add(Dense(1, activation='sigmoid'))

model.add(Dense(200, input_dim=280, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(70, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())
#model.fit(X_train, y_train, nb_epoch=3, batch_size=64)
# Final evaluation of the model
#scores = model.evaluate(X_test, y_test, verbose=0)
#print("Accuracy: %.2f%%" % (scores[1]*100))


model = load_model("ALL_BALANCED.10epochs.DENSE_3x_withDropout.model") ###ALL_minus.Desulfosporosinus.10epochs.DENSE_3x_withDropout.model") ###ALL_mock.10epochs.DENSE_3x_withDropout.model")  ###ALL_mock.10epochs.Dense.model")



class_predict = model.predict(X_test, batch_size=32, verbose=1)

print(class_predict)

plt.plot(class_predict)
plt.show()


scores = model.evaluate(X_test, Y_test, verbose=1)
print("Accuracy training ALL_mock.10epochs.DENSE_3x.model, testing Nocardiopsis: %s  %.2f%%" % (model.metrics_names[1], scores[1]*100))
print("Accuracy training ALL_mock.10epochs.DENSE_3x.model, testing Nocardiopsis: %s  %.2f%%" % (model.metrics_names[0], scores[0]*100))


'''
model.fit(X, dummy_y,
                epochs=50,
                batch_size=256,
                shuffle=True,
                validation_data=(X,dummy_y))

model.save("ALL_mock.50epochs.model")

scores = model.evaluate(X_test, dummy_y_test, verbose=1)
print("Accuracy training ALL_mock, testing Nocardiopsis: %.2f%%" % (scores[1]*100))


estimator = KerasClassifier(build_fn=baseline_model, epochs=50, batch_size=256, verbose=0)
print("estimator = KerasClassifier(build_fn=baseline_model, epochs=50, batch_size=256, verbose=0)")
kfold = KFold(n_splits=3, shuffle=True, random_state=seed)
print("kfold = KFold(n_splits=3, shuffle=True, random_state=seed)")

results = cross_val_score(estimator, X, dummy_y, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
'''


