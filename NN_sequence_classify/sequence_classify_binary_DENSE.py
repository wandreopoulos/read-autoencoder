###
###https://raw.githubusercontent.com/fchollet/keras/master/examples/mnist_cnn.py
###https://github.com/fchollet/keras/blob/master/examples/mnist_cnn.py
###


'''
Trains a simple convnet on a DNA sequence dataset with 2 classes.
The goal is to separate 2 classes that are annotated in the training data.
'''

from __future__ import absolute_import, division, print_function

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d

import pandas
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.preprocessing import sequence
from keras.layers import LSTM

from keras.layers.embeddings import Embedding
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline

import matplotlib.pyplot as plt

def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]

#____________
###Train data fasta

x=[]
xp2 = []
###with open("ALL_BALANCED.fasta") as f:
###with open("PLASMID/ALL_BALANCED.fasta") as f:
with open("MITO/main_mito_BALANCED.1024.fasta") as f:
  for l in f:
    if not l.startswith(">"):
      #a=map(np.float32, list(l.strip()))
      a=list(l.strip())
      while len(a) < 1024:
         a.append("N")
      xp = one_hot_encode(np.array(a))
      xp2.append(np.reshape(xp,4096))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=4096)
X_train = xn

#______________
####Test data fasta

x=[]
xp2 = []
with open("PLASMID/Desulfosporosinus.fasta") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 70:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,280))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=280)
X_test = xn

#______________
###Train data classes
###with open("ALL_BALANCED.classes") as f:
###with open("MITO/main_mito_BALANCED.classes") as f:
###with open("PLASMID/ALL_BALANCED.classes") as f:
with open("MITO/main_mito_BALANCED.1024.classes") as f:
   content = f.readlines()

###print("Training ALL_BALANCED.classes")
print("Training MITO/main_mito_BALANCED.classes")

content = [x.strip() for x in content]
Y = np.array(content)

content2 = [int(x) for x in content]
Y_train = np.array(content2)

#______________
####Test data classes
with open("PLASMID/Desulfosporosinus.classes") as f:
   content = f.readlines()

print("Testing Desulfosporosinus.classes")

content = [x.strip() for x in content]
Y = np.array(content)

content2 = [int(x) for x in content]
Y_test = np.array(content2)

#______________
####Separate training and testing
import random
X_train_tmp = X_train
Y_train_tmp = Y_train
X_train = []
Y_train = []
X_test = []
Y_test = []

for i in xrange( len(Y_train_tmp) ):
   ran = random.random()
   if ran <= 0.1:
      X_test.append(X_train_tmp[i])
      Y_test.append(Y_train_tmp[i])
   else:
      X_train.append(X_train_tmp[i])
      Y_train.append(Y_train_tmp[i])

X_train = np.array(X_train)
Y_train = np.array(Y_train)
X_test = np.array(X_test)
Y_test = np.array(Y_test)

#______________

batch_size = 1000
num_classes = 2
epochs = 10

# input image dimensions
img_rows, img_cols = 1024, 4

#    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
input_shape = (1, img_rows, img_cols)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = keras.utils.to_categorical(Y_train, num_classes)
Y_test = keras.utils.to_categorical(Y_test, num_classes)



###https://github.com/fchollet/keras/issues/3945
from keras import backend as K
K.set_image_dim_ordering('tf')

model = Sequential()
model.add(Conv2D(32, kernel_size=(1, 4),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (1, 4), activation='relu'))
model.add(MaxPooling2D(pool_size=(1, 2)))
model.add(Dropout(0.2))
model.add(Flatten())
model.add(Dense(70, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.mean_squared_error,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(X_train, Y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1, shuffle=True,
          validation_data=(X_test, Y_test))

###model.save("PLASMID/ALL_BALANCED.20epochs.Conv2D_DENSE_withDropout.plasmids.model")
model.save("MITO/main_mito_BALANCED.10epochs.Conv2D_DENSE_withDropout.1024.model")


score = model.evaluate(X_test, Y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


print("______________________Predictions:______________________")

class_predict = model.predict(X_test, batch_size=32, verbose=1)

print(class_predict)


# plt.plot(class_predict)
# plt.show()
# 
# A=[]
# for i in class_predict:
# ...    A.append(i[0])
# plt.scatter(range(101111),A)
# plt.show()
# 
# B=[]
# for i in class_predict:
# ...     B.append(i[1])
# plt.scatter(range(101111),B)
# plt.show()

