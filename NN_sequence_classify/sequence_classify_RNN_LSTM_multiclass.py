#https://www.quora.com/How-can-recurrent-neural-networks-be-used-for-classification
#https://github.com/fchollet/keras/blob/master/examples/imdb_bidirectional_lstm.py


'''Train a Bidirectional LSTM on the IMDB sentiment classification task.

Output after 4 epochs on CPU: ~0.8146
Time per epoch on CPU (Core i7): ~150s.
'''

from __future__ import print_function
import numpy as np

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional
from keras.datasets import imdb
from keras.utils import np_utils





from __future__ import absolute_import, division, print_function
import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d


def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]






x=[]
xp2 = []

#f.close()
#f=open('all.fastq', 'r')
#for l in f.readlines():
#count=1
with open("all.fastq") as f:
  for l in f:
    #a=map(np.float32, list(l.strip()))
    a=list(l.strip())
    while len(a) < 150:
       a.append("N")
    xp = one_hot_encode(np.array(a))
    xp2.append(np.reshape(xp,600))
    #count
    #count += 1
    #x.append(a)
    #print(str(a) + "  %s" % (len(a)))

#xn=np.array(x)
#xn=xn/4.0
#xp = one_hot_encode(np.array(x))
#xp2 = []
#for i in xp:
#    xp2.append(np.reshape(i,600))

xn = np.array(xp2)
xn = sequence.pad_sequences(xn, maxlen=600)
xn[0]
#f.close()








# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# load dataset
#dataframe = pandas.read_csv("iris.csv", header=None)
#dataset = dataframe.values
#X = dataset[:,0:4].astype(float)
X = xn
#Y = dataset[:,4]
#Y = np.ones(len(X))
#TODO check if num classes = the value in model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))
with open("all.categories") as f:
   content = f.readlines()

content = [x.strip() for x in content]
Y = np.array(content)


# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)




max_features = 20000
# cut texts after this number of words
# (among top max_features most common words)
maxlen = 600
batch_size = 32

'''
print('Loading data...')
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
print(len(x_train), 'train sequences')
print(len(x_test), 'test sequences')

print("Pad sequences (samples x time)")
x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
print('x_train shape:', x_train.shape)
print('x_test shape:', x_test.shape)
y_train = np.array(y_train)
y_test = np.array(y_test)
'''

model = Sequential()
#model.add(Dense(600, input_dim=600, kernel_initializer='normal', activation='relu'))
#model.add(Dense(2, kernel_initializer='normal', activation='sigmoid'))
model.add(Embedding(max_features, 128, input_length=maxlen))
model.add(Bidirectional(LSTM(64)))
model.add(Dropout(0.5))
#model.add(Dense(2, activation='sigmoid'))
model.add(Dense(26, kernel_initializer='normal', activation='sigmoid'))

# try using different optimizers and different optimizer configs
model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])

print('Train...')
model.fit(X, dummy_y,
          batch_size=batch_size,
          epochs=4,
          validation_data=[X, dummy_y])###x_test, y_test])

scores = model.evaluate(X, dummy_y, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))

