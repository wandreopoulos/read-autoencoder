###
###https://raw.githubusercontent.com/fchollet/keras/master/examples/mnist_cnn.py
###https://github.com/fchollet/keras/blob/master/examples/mnist_cnn.py
###


'''
Trains LSTM and a simple convnet on a DNA sequence dataset with 2 classes.
The goal is to separate 2 classes that are annotated in the training data.
'''

from __future__ import absolute_import, division, print_function

import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

import numpy as np
import sys
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.signal import correlate2d

from argparse import ArgumentParser
import re

import pandas
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.preprocessing import sequence
from keras.layers import LSTM, Input, concatenate

from keras.layers.embeddings import Embedding
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D

from keras.models import load_model

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline

import matplotlib.pyplot as plt

import Plotter_Mito

def one_hot_encode(sequences):
    sequence_length = len(sequences[0])
    integer_type = np.int8 if sys.version_info[
        0] == 2 else np.int32  # depends on Python version
    integer_array = LabelEncoder().fit(np.array(('ACGTN',)).view(integer_type)).transform(
        sequences.view(integer_type)).reshape(len(sequences), sequence_length)
    one_hot_encoding = OneHotEncoder(
        sparse=False, n_values=5, dtype=integer_type).fit_transform(integer_array)
    return one_hot_encoding.reshape(
        len(sequences), 1, sequence_length, 5).swapaxes(2, 3)[:, :, [0, 1, 2, 4], :]


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":
    usage = "module load deeplearning ; python sequence_classify_LSTM_DENSE_Jan.py\nExamples: python sequence_classify_LSTM_DENSE_Jan.py -t PLASMID/PLASMIDS_BALANCED.NONPLASMIDS.fasta -m PLASMID/PLASMIDS_BALANCED.NONPLASMIDS.fasta.model -e /global/dna/shared/data/functests/Assembly/Meta/mock_community/references/Meiothermus_silvanus_DSM_9946.fasta  -lb plasmid  -cr\n  andreopo@cori01:/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify> python sequence_classify_LSTM_DENSE_Jan.py -t MITO/CROSS-VALIDATION_allfungalstd/ALL.maingenomes.mitochondria.scaffolds.BALANCED.fasta  -m  MITO/CROSS-VALIDATION_allfungalstd/ALL.maingenomes.mitochondria.scaffolds.BALANCED.fasta.MODEL  -e  MITO/CROSS-VALIDATION_allfungalstd/ALL.mitochondria.scaffolds.fasta   -lb mito  -cr -ep 10"
    ###Input parameters: file names train test model, flag cross-validation, dropout, vector length.
    parser = ArgumentParser(usage = usage)
    parser.add_argument("-m", "--model", dest="model", help = "Model", required=True)
    parser.add_argument("-e", "--test", dest="test", help = "Test", required=True)
    parser.add_argument("-d", "--dropout", dest="dropout", help = "Dropout", required=False)
    parser.add_argument("-l", "--length", dest="length", help = "Vector length", required=False)
    parser.add_argument("-sn", "--sin-neu", dest="single_neuron_out", default=False, action="store_true", help = "Turn on single output neuron")
    parser.add_argument("-lb", "--label", dest="label", help = "Label", required=True)
    parser.add_argument("-ep", "--epochs", dest="epochs", help = "Epochs", required=False)

    
    epochs = 10
    dropout = 0.2
    length = 150
    cross_val = False
    single_neuron_out = False
    train_file = "PLASMID/PLASMIDS_BALANCED.NONPLASMIDS.fasta"
    test_file = "/global/dna/shared/data/functests/Assembly/Meta/mock_community/references/Meiothermus_silvanus_DSM_9946.fasta"
    model_file = "PLASMID/PLASMIDS_BALANCED.NONPLASMIDS.fasta.model"
    train_class = ""
    test_class = ""
    label="plasmid"
    
    # parse args
    args = parser.parse_args()

    if args.model:
       model_file = args.model

    if args.test:
       test_file = args.test

    if args.dropout:
       dropout = float(args.dropout)

    if args.length:
       length = int(args.length)

    if args.label:
       label = args.label

    if args.epochs:
       epochs = int(args.epochs)

    if args.single_neuron_out:
       single_neuron_out = args.single_neuron_out

    print("Epochs: %s" % (epochs) )
    print("Dropout: %s" % (dropout) )
    print("Length: %s" % (length) )
    print("Single-neuron-out: %s" % (single_neuron_out) )

    #______________
    ####Test data fasta
    
    contigs = []
    starts = []
    x=[]
    xp2 = []
    seq = ""
    content2 = []
    header = ""
    with open(test_file) as f:
        for line in f:
            line = line.strip()
            if line.startswith(">"):
                contigs.append(line)
                starts.append(len(content2))
                if (seq != ""):
                   clas = 1 if header.find(label) > -1 else 0
                   asss = [seq[i:i+length] for i in range(0, len(seq), length)]
                   for ass in asss:
                       a = list(ass)
                       while len(a) < length:
                          a.append("N")
                       xp = one_hot_encode(np.array(a))
                       xp2.append(np.reshape(xp,length*4))
                       content2.append(clas)
                seq = "";
                header = line
            elif len(line) > 2:
                line = line.upper()
                line = re.sub('[^ATCG]', '', line)
                seq += line
        if (seq != ""):
           clas = 1 if header.find(label) > -1 else 0
           asss = [seq[i:i+length] for i in range(0, len(seq), length)]
           for ass in asss:
               a = list(ass)
               while len(a) < length:
                  a.append("N")
               xp = one_hot_encode(np.array(a))
               xp2.append(np.reshape(xp,length*4))
               content2.append(clas)
        '''
        #a=map(np.float32, list(l.strip()))
        a=list(l.strip())
        while len(a) < 70:
           a.append("N")
        xp = one_hot_encode(np.array(a))
        xp2.append(np.reshape(xp,280))
        '''
    
    xn = np.array(xp2)
    xn = sequence.pad_sequences(xn, maxlen=length*4)
    X_test = xn
    
    #______________
    ####Test data classes
    '''
    with open(test_class) as f:
       content = f.readlines()
    
    content = [x.strip() for x in content]
    Y = np.array(content)
    
    content2 = [int(x) for x in content]
    '''
    Y_test = np.array(content2)
    
    #______________
    print("Testing data:")
    print(X_test.shape[0], 'X_test samples')
    print(Y_test.shape[0], 'Y_test samples')

    #______________
    
    batch_size = 1000
    num_classes = 2
    ###epochs = 20
    
    # input image dimensions
    img_rows, img_cols = length, 4
    
    #    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0],  img_rows, img_cols)
    input_shape = (img_rows, img_cols)
    
    X_test = X_test.astype('float32')
    print("POST-reshape:")
    print('X_test shape:', X_test.shape)
    print(X_test.shape[0], 'test samples')
    
    # convert class vectors to binary class matrices
    Y_test_cat = keras.utils.to_categorical(Y_test, num_classes)
    

    
    ###https://github.com/fchollet/keras/issues/3945
    #from keras import backend as K
    #K.set_image_dim_ordering('tf')
    
    
    inputA = Input(shape=input_shape, name='DNA_seq')
    print('build_model inpA:',inputA.get_shape())
            
    lstm_A1=30
    lstm_A2=15
    dens_n2=5
    dens_n1=dens_n2*2
    densAct='relu'
    
    #layerDropFrac=0.1+ (args.arrIdx%5)/10.
    layerDropFrac=0.2
    recDropFrac=layerDropFrac/2.
    
    print('Dens act=',densAct,' recurDropFrac=',recDropFrac,' layerDropFrac=',layerDropFrac)
    
    netA= LSTM(lstm_A1, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='A1_%d'%lstm_A1,return_sequences=True) (inputA)
    
    netA= LSTM(lstm_A2, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='A2_%d'%lstm_A2) (netA)
    
    net=Dense(dens_n1, activation=densAct, name='D_%d'%dens_n1)(netA)
    net=Dropout(layerDropFrac)(net)
    net=Dense(dens_n2, activation=densAct, name='D_%d'%dens_n2)(net)
    net=Dropout(layerDropFrac)(net)
    outputs=None
    if single_neuron_out:
       outputs=Dense(1, activation='sigmoid', name='decision_2')(net)  # predicts only 0/1
    else:
       outputs=Dense(num_classes, activation='sigmoid', name='decision_1')(net)
    model = Model(inputs=inputA, outputs=outputs)

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary() # will print
    
    model = load_model(model_file) 
    
    if single_neuron_out:
        score = model.evaluate(X_test, Y_test, verbose=0)
    else:
        score = model.evaluate(X_test, Y_test_cat, verbose=0)

    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    
    
    print("______________________Predictions:______________________")
    
    class_predict = model.predict(X_test, batch_size=batch_size, verbose=1)
    
    if single_neuron_out:
      score_thr = 0.7
    else:
      score_thr = 0.5

    print(class_predict)
    
    predict = [None] * len(class_predict)
    count = 0
    if single_neuron_out:
      for i in class_predict:
        if i < score_thr:
          predict[count] = 0
        else:
          predict[count] = 1
        count += 1
    else:
      for i in class_predict:
        if i[0] > i[1]:
          predict[count] = 0
        else:
          predict[count] = 1
        count += 1

    print(predict)
          
    if not cross_val: ###if cross_val X_test and Y_test have been overwritten and contig info lost
        predict_pos = open("predict_pos.txt", "w")
        predict_neg = open("predict_neg.txt", "w")
        start = 0
        end = 0
        for i in range(0, len(contigs)):
           start = starts[i]
           end = starts[i+1] if i < len(contigs)-1 else len(predict)
           contig = contigs[i]
           count_pos = 0
           count_neg = 0
           for j in range(start, end):
              if predict[j]:
                  count_pos += 1
              else:
                  count_neg += 1
           if count_pos > count_neg:
              predict_pos.write(contig + "\n")
           else:
              predict_neg.write(contig + "\n")
        predict_pos.close()
        predict_neg.close()


    FPs = 0 ###count where predict is 1 but Y_test is 0
    FNs = 0 ###count where predict is 0 but Y_test is 1
    for i in range(0, len(predict)):
      if predict[i] and not Y_test[i]:
         FPs += 1
      if not predict[i] and Y_test[i]:
         FNs += 1
    
    print("X_test %s, Y_test_cat %s" % ( len(X_test), len(Y_test_cat) ))
    print ("FPs %s , FNs %s" % ( FPs , FNs ))

    ppp = Plotter_Mito(args)
    ppp.plot_confusion_matrix(Y_test,predict,score_thr,figId=13,title=train_file)

    if single_neuron_out:
       ppp.plot_labeled_scores(Y_test,class_predict,score_thr,figId=13,title=train_file)

    # plt.plot(class_predict)
    # plt.show()
    # 
    # A=[]
    # for i in class_predict:
    # ...    A.append(i[0])
    # plt.scatter(range(101111),A)
    # plt.show()
    # 
    # B=[]
    # for i in class_predict:
    # ...     B.append(i[1])
    # plt.scatter(range(101111),B)
    # plt.show()
    
